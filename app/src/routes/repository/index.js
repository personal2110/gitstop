import { h } from 'preact';
import {useEffect, useState} from "preact/hooks";
import style from './style.css';

// Note: `repo` comes from the URL, courtesy of our router
const Repository = ({ platform, owner, repo }) => {
	var SEARCH, TREE;
	var URL;

	const [items, setItems] = useState([]);
	const [tree, setTree] = useState([]);


	if (platform == 'github'){
		SEARCH = 'https://api.github.com/repos';
		TREE = 'git/trees/master'

		useEffect(() => {
			fetch(`${SEARCH}/${owner}/${repo}`)
				.then(res => res.json())
				.then(data => setItems((data) || []))
			fetch(`${SEARCH}/${owner}/${repo}/${TREE}`)
				.then(res => res.json())
				.then(data => setTree((data && data.tree) || []));
		}, []);

		URL = items.html_url;

	} else if (platform == 'gitlab') {
		SEARCH = 'https://gitlab.com/api/v4/projects';
		TREE = 'repository/tree'

		useEffect(() => {
			fetch(`${SEARCH}/${owner}%2F${repo}`)
				.then(res => res.json())
				.then(data => setItems((data) || []))
			fetch(`${SEARCH}/${owner}%2F${repo}/${TREE}`)
				.then(res => res.json())
				.then(data => setTree((data) || []));
		}, []);

	} else if (platform == 'gitea'){
		SEARCH = 'https://codeberg.org/api/v1/repos';
		TREE = 'git/trees/master'

		useEffect(() => {
			fetch(`${SEARCH}/${owner}/${repo}`)
				.then(res => res.json())
				.then(data => setItems((data) || []))
			fetch(`${SEARCH}/${owner}/${repo}/${TREE}`)
				.then(res => res.json())
				.then(data => setTree((data) || []));
		}, []);
	}
	

	return (
		<div class={style.repository}>
			<h1><a href={'/profile/' + platform + '/' + owner}>{owner}</a>/{repo}</h1>
			<p>{items.description}</p>
			<p><a href={URL}>View on {platform}</a></p>

			<div class="list">
				{tree.map(result => (
					<Result {...result} />
				))}
			</div>
		</div>
	);
}

const Result = result => (
	<div class="list-item">
		<div>
			<a href={result.path} target="_blank" rel="noopener noreferrer">
				{result.path}
			</a>
		</div>
		<p>{result.description}</p>
	</div>
);

export default Repository;
