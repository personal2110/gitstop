import { h } from 'preact';
import style from './style.css';

import { render, Component } from "preact";
import { route } from 'preact-router';

class SearchForm extends Component {
	state = { text: "Example"};

	onSubmit = e => {
		route('/search/' + this.state.text)
		e.preventDefault();
	}

	updateText = e => {
		this.setState({ text: e.target.value });
	}

	render(_, { text }) {
		return (
		 	<form onSubmit={this.onSubmit} action="javascript:">
		 		<b> Search: </b>
				<input type="text" value={ text } onInput={this.updateText}/>
				<button type="submit">Search</button>
			</form>
		);
	}
}

const Home = () => (
	<div class={style.home}>
		<h1>Welcome!</h1>
		<h3>Welcome to GitStop, a simple, lightweight and privacy respecting git aggregator</h3>
		<p>To get started head over to your <a href='/profile'>profile</a> and setup what to pull from, or search for something below</p>
		<SearchForm />
	</div>
);

export default Home;
