import { h } from 'preact';
import {useEffect, useState} from "preact/hooks";
import { render, Component } from "preact";
import style from './style.css';


class SettingsForm extends Component {
	state = { gl: localStorage.getItem('GL'), gt: localStorage.getItem('GT')};

	onSubmit = e => {
		localStorage.setItem('GL', this.state.gl)
		localStorage.setItem('GT', this.state.gt)
		alert("Settings Updated");
		e.preventDefault();
	}

	updateTextGL = e => {
		this.setState({ gl: e.target.value });
	}

	updateTextGT = e => {
		this.setState({ gt: e.target.value });
	}

	render(_, { gl, gt }) {
		return (
		 	<form onSubmit={this.onSubmit} action="javascript:">
		 		<b> GitLab Instance: </b>
				<input type="text" value={ gl } onInput={this.updateTextGL}/>
				<b> Gitea Instance: </b>
				<input type="text" value={ gt } onInput={this.updateTextGT}/>
				<button type="submit">Apply Changes</button>
			</form>
		);
	}
}

// Note: `user` comes from the URL, courtesy of our router
const Profile = ({ platform, user })  => {

	if (platform === "me"){
		console.log(localStorage.getItem('SourcesEnabled'))

		if (localStorage.getItem('SourcesEnabled') == null){
			console.log("Settings not created, setting them now")
			localStorage.setItem('SourcesEnabled', ['http://github.com', 'http://gitlab.com', 'http://codeberg.org'])
		}

		if (localStorage.getItem('GL') == null){
			console.log("Gitlab Settings not created, setting them now")
			localStorage.setItem('GL', 'http://gitlab.com')
		}

		if (localStorage.getItem('GT') == null){
			console.log("Gitea Settings not created, setting them now")
			localStorage.setItem('GT', 'http://codeberg.org')
		}

		return (
			<div class={style.profile}>
				<h1>Hello there!</h1>
				<p>Here you can edit your settings</p>
				<h3>Instances:</h3>
				<SettingsForm />
			</div>
		);
	} else {

		var about, avatar_url, url, repos;
		var SEARCH;

		switch (platform){
			case 'github':
				SEARCH = 'https://api.github.com/users';

				const [items, setItems] = useState([]);
				const [itemsA, setItemsA] = useState([]);


				useEffect(() => {
					fetch(`${SEARCH}/${user}`)
						.then(res => res.json())
						.then(data => setItems((data) || []))
				}, []);

				about = items.bio;
				avatar_url = items.avatar_url;
				url = items.html_url;
				break
			case 'gitlab':
				SEARCH = 'https://gitlab.com/api/v4/users';

				const [itemsGL, setItemsGL] = useState([]);

				useEffect(() => {
					fetch(`${SEARCH}?username=${user}`)
						.then(res => res.json())
						.then(data => setItemsGL((data) || []))
				}, []);

				about = 'None';
				avatar_url = itemsGL.avatar_url;
				url = itemsGL.web_url;
				repos = [];
				break
		}

		return (
			<div class={style.profile}>
				<h1>Profile of {user} on {platform}</h1>
				<h2>About Me</h2>
				<p>{about}</p>
				<h2>Repositories</h2>
				<i><a href={url}> View {user} on {platform}</a></i>
			</div>
		);
	}
}

export default Profile;
