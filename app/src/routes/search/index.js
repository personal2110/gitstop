import { h } from 'preact';
import {useEffect, useState} from "preact/hooks";
import style from './style.css';

// Note: `repo` comes from the URL, courtesy of our router
const SearchAcross = ({ query }) => {
	var SEARCH_GH, SEARCH_GL;

	const [itemsGH, setItemsGH] = useState([]);
	const [itemsGL, setItemsGL] = useState([]);


	SEARCH_GH = "https://api.github.com/search/repositories"
	SEARCH_GL = "https://gitlab.com/api/v4/projects"

	useEffect(() => {
		fetch(`${SEARCH_GH}?q=${query}&sort=stars`) // Sort can be default (`best-match`), `stars` or `updated`
			.then(res => res.json())
			.then(data => setItemsGH((data && data.items) || []));
		fetch(`${SEARCH_GL}?search=${query}`)
			.then(res => res.json())
			.then(data => setItemsGL(data));
	}, []);

	return (
		<div>
			<h1 style="text-align:center; font-weight: 200">Results</h1>
			<div class="list">
				{itemsGH.map(result => (
					<ResultGH {...result} />
				))}
			</div>
			<div class="list">
				{itemsGL.map(result => (
					<ResultGL {...result} />
				))}
			</div>
		</div>
	);
}

const ResultGH = result => (
	<div class="list-item">
		<div>
			<a href={"/repository/github/" + result.full_name} target="_blank" rel="noopener noreferrer">
				{result.full_name}
			</a>
			{' - '}
			<strong>⭐️{result.stargazers_count}</strong>
		</div>
		<p>{result.description}</p>
	</div>
);

const ResultGL = result => (
	<div class="list-item">
		<div>
			<a href={"/repository/gitlab/" + result.path_with_namespace} target="_blank" rel="noopener noreferrer">
				{result.path_with_namespace}
			</a>
			{' - '}
			<strong>⭐️{result.star_count}</strong>
		</div>
		<p>{result.description}</p>
	</div>
);

export default SearchAcross;
