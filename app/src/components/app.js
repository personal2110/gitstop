import { h } from 'preact';
import { Router } from 'preact-router';
import { route } from 'preact-router';
import { Component } from 'preact';

import Header from './header';

// Code-splitting is automated for `routes` directory
import Home from '../routes/home';
import Profile from '../routes/profile';
import Repository from '../routes/repository';
import SearchAcross from '../routes/search';


class Redirect extends Component {
  componentWillMount() {
    route(this.props.to, true);
  }

  render() {
    return null;
  }
}

const App = () => (
	<div id="app">
		<Header />
		<Router>
			<Home path="/" />
			<Profile path="/profile/" platform="me" />
			<Profile path="/profile/:platform/:user" />
			<SearchAcross path="/search" query=""/>
			<SearchAcross path="/search/:query" />
			<Repository path="/repository/:platform/:owner/:repo" />			
		</Router>
	</div>
)

export default App;
