import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style.css';

const Header = () => (
	<header class={style.header}>
		<h1><Link href="/" style="color: #FFF;  text-decoration: none;">GitStop</Link></h1>
		<nav>
			<Link activeClassName={style.active} href="/">Home</Link>
			<Link activeClassName={style.active} href="/search">Explore</Link>
			<Link activeClassName={style.active} href="/profile">Profile</Link>
		</nav>
	</header>
);

export default Header;
