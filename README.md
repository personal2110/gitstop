# GitStop

## Goals
* Clean and Simple UI
* Lightweight 
* Privacy respecting with no tracking/analytics
* Searches through multiple different git sources (Such as GitHub, GitLab, Gitea[^1], etc.)
* Self-Hostable
* Proxy request in the future to provide more privacy (even though not currently proxied, by using the API, far less data is given up)
* Pass Computer Science

## Development Stack
* Currently using [Preact](https://preactjs.com/)

## TODO
[x] Basic Routing
[ ] Basic Repository Info
	[x] Github
	[x] GitLab
	[x] Gitea[^2]
[ ] Basic User Info
	[x] Github
	[x] GitLab
	[ ] Gitea
[ ] Searching Across Platforms by Stars
	[x] Github
	[x] GitLab[^3]
	[ ] Gitea
[ ] Styling
	[ ] Github
	[ ] GitLab
	[ ] Gitea
[ ] Serve Files via proxy (Very Advanced, probably should be done with better backend such as Rust or Go)
	[ ] Github
	[ ] GitLab
	[ ] Gitea

# Improvements
* Use a better more efficent backend like Rust or Go as seen in projects such as [Libreddit](https://github.com/spikecodes/libreddit)
* Remove Javascript to improve Privacy as seen in projects such as Libreddit, Invidious, Piped etc.

# Running Locally
See `app/README.md`

TLDR:
* `pip install nodeenv`
* `nodeenv -node=16.17.9 nenv`
* `source nenv/bin/activate`
* `cd app`
* `npm ci`
* `npm run dev`


[^1]: Including support for any Gitea instance (eg. Codeberg).
[^2]: Note: The gitea instance must support CORS or no data will be returned
[^3]: GitLab API is kind of broken without an API key so it doesn't work properly currently